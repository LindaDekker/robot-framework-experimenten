*** Settings ***


*** Test Cases ***
None of varNone
    [Documentation]  Check the difference in the word None and the variable None in a if statement
    ...              Conclusion: variable with text None does not work. variable None is the way to go.  
    ...              In the if statement 'is not None' and 'is not var None' works both
    ${test}    Set Variable  None  
    ${test2}   Set Variable  ${None}
    
    Log  All the if statements should not be executed:

    IF  $test is not None  
        Log  1. 'is not None' combined with text None    WARN
    END
    IF  $test is not ${None}
        Log  2. 'is not varNone' combined with text None    WARN
    END
    IF  $test2 is not ${None}
        Log  3. 'is not varNone' combined with var None    WARN
    END
    IF  $test2 is not None
        Log  4. 'is not None' combined with var None    WARN
    END
    
    # RESULT
    # [ WARN ] 'is not None' combined with text None                                
    # [ WARN ] 'is not varNone' combined with text None       