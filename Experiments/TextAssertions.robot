*** Settings ***
Library         SeleniumLibrary

Test Setup      Open Browser  https://www.demoblaze.com  chrome
Test Teardown   Close All Browsers


*** Test Cases ***
1. Voorbeeld
    Element Should Contain    //a[@id="cat"]    CATEGORIES   #Contains 

2. Voorbeeld
    Element Text Should Be    //a[@id="cat"]   CATEGORIES    #Exacte text

3. Voorbeeld
    Page Should Contain Element    //a[@id="cat" and text()="CATEGORIES"]

1. Voorbeeld - Onjuiste tekst
    Element Should Contain    //a[@id="cat"]    CATEGORIESSS   #Contains 

2. Voorbeeld - Onjuiste tekst
    Element Text Should Be    //a[@id="cat"]   CATEGORIESSS    #Exacte text

3. Voorbeeld - Onjuiste tekst
    Page Should Contain Element    //a[@id="cat" and text()="CATEGORIESSS"]

1. Voorbeeld - element bestaat niet
    Element Should Contain    //a[@id="cattt"]    CATEGORIES   #Contains 

2. Voorbeeld - element bestaat niet
    Element Text Should Be    //a[@id="cattt"]   CATEGORIES    #Exacte text

3. Voorbeeld - element bestaat niet
    Page Should Contain Element    //a[@id="cattt" and text()="CATEGORIES"]
